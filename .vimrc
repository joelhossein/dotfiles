" Set number of lines
set nu


" Set compatibility to Vim only.
set nocompatible
set nolist


" Turn on syntax highlighting.
syntax on


" Highlight matching pairs of brackets. Use the '%' character to jump between them.
set matchpairs+=<:>


" Speed up scrolling in Vim
set ttyfast


" Display 5 lines above/below the cursor when scrolling with a mouse.
set scrolloff=5


" Fixes common backspace problems
set backspace=indent,eol,start


set formatoptions-=cro "tcqrn1"
set tabstop=4
set shiftwidth=4
set softtabstop=4
set expandtab
set noshiftround
set smarttab
set smartindent
set autoindent
" set showtabline=4
set hidden
set fileencoding=utf-8
set ruler
set mouse=a
" set cursorline
set splitbelow
set splitright
set clipboard=unnamedplus
set background=dark
set autochdir
syntax enable
filetype on
filetype indent on
filetype plugin on


" Highlight matching search patterns
set hlsearch
" Enable incremental search
set incsearch
" Include matching uppercase words with lowercase search term
set ignorecase
" Include only uppercase words with uppercase search term
set smartcase


" Toggle Commentary
nnoremap <C-c> :Commentary<CR>
xnoremap <C-c> :Commentary<CR>


let mapleader = " "

"Shortcut for NerdTree"
nmap <leader>n :NERDTreeToggle<cr>


" Better window navigation
nnoremap <C-h> <C-w>h
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l

" Remap scroll with smooth_scroll plugin
noremap <silent> <c-u> :call smooth_scroll#up(&scroll, 15, 2)<CR>
noremap <silent> <c-d> :call smooth_scroll#down(&scroll, 15, 2)<CR>
noremap <silent> <c-b> :call smooth_scroll#up(&scroll*2, 15, 4)<CR>
noremap <silent> <c-f> :call smooth_scroll#down(&scroll*2, 15, 4)<CR>


call plug#begin('~/.vim/plugged')

Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }
Plug 'tpope/vim-commentary'
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'honza/vim-snippets'
Plug 'pangloss/vim-javascript'    " JavaScript support
Plug 'leafgarland/typescript-vim' " TypeScript syntax
Plug 'jiangmiao/auto-pairs'
Plug 'alvan/vim-closetag'
Plug 'ap/vim-css-color'
Plug 'terryma/vim-multiple-cursors'
Plug 'terryma/vim-smooth-scroll'

call plug#end()

colorscheme slate
